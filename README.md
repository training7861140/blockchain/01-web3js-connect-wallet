[TOC]
# 01-web3js-connect-wallet

Xây dựng một trang web đơn giản bằng html,css,js để connect wallet và tương tác với Blockchain (Smart Contract)

Các nội dung:

### 1/ Init a simple project to connect wallet
- https://docs.metamask.io/wallet/how-to/get-started-building/set-up-dev-environment/

```
simple-dapp/
├─ src/
│  ├─ index.js
├─ dist/
│  ├─ index.html
```
### 2/ Config webpack (A module bundler)
- https://webpack.js.org/guides/getting-started/

- Init npm
```bash
npm init -y
```
- Install webpack
```bash
npm install webpack webpack-cli --save-dev
```
- Init webpack config `webpack.config.js`

```js
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  mode: 'development'
};

```
- Build

```
 npx webpack
```
GOT ERROR:
```
 01-web3js-connect-wallet git:(develop) ✗  npx webpack
asset main.js 8.23 KiB [emitted] (name: main)
runtime modules 2.32 KiB 2 modules
./src/index.js 2.17 KiB [built] [code generated]

ERROR in ./src/index.js 5:0-63
Module not found: Error: Can't resolve '@metamask/detect-provider' in '/Users/bapvn/Documents/BAP/RDC/training/blockchain/01-web3js-connect-wallet/src'
```

- Instal dependencies

```
 npm install @metamask/detect-provider
```

- Re-build:
```bash
 npx webpack --config webpack.config.js
```

result:
```bash
➜  01-web3js-connect-wallet git:(develop) ✗ npx webpack --config webpack.config.js
asset main.js 18.1 KiB [emitted] (name: main)
runtime modules 2.97 KiB 5 modules
cacheable modules 10.3 KiB
  ./src/index.js 2.17 KiB [built] [code generated]
  ./node_modules/@metamask/detect-provider/dist/index.js 8.18 KiB [built] [code generated]
webpack 5
```

- Run web local with http protocol

> [http-server](https://www.npmjs.com/package/http-server): a simple static HTTP server

```bash
➜  01-web3js-connect-wallet git:(develop) ✗ npx http-server
Starting up http-server, serving ./

http-server version: 14.1.1

http-server settings:
CORS: disabled
Cache: 3600 seconds
Connection Timeout: 120 seconds
Directory Listings: visible
AutoIndex: visible
Serve GZIP Files: false
Serve Brotli Files: false
Default File Extension: none

Available on:
  http://127.0.0.1:8080
  http://192.168.1.140:8080
Hit CTRL-C to stop the server
```

Visit http://127.0.0.1:8080/dist/


### 3/ Run a development blockchain network
- https://docs.metamask.io/wallet/how-to/get-started-building/run-devnet/

Sử dụng `Hardhat` chạy một mạng Ethereum network giải lập ở local để phục vụ test. Trên mạng deveopment blockchain này chúng ta có sẵn wallet và coin để test các chức năng.

#### 3.1/ Set up a Hardhat project.

```
mkdir sameple-hardhat-project
cd sameple-hardhat-project
npm init -y
npm install --save-dev hardhat
```

Create a Hardhat project:
```
npx hardhat init
```

#### 3.2/ Create a new MetaMask seed phrase specifically for development.
- Cài đặt metamask và tạo tài khoản mới, lưu lại `seed phrase` (hay còn gọi là mnemonic code)

#### 3.3/ In your hardhat.config.js file, specify a networks configuration with a hardhat network. In this networks.hardhat configuration:

Alternatively, to prevent committing your seed phrase, we recommend adding your seed phrase to a `.env` file and using the process.env global variable in `hardhat.config.js.`

- Install `dotenv` module

```bash
npm install dotenv --save
```
- adding your seed phrase to a `.env` file:

```
SEED_PHRASE=your seed phares
```

- In your `hardhat.config.js` file, specify a networks configuration with a hardhat network. In this networks.hardhat configuration:

```
module.exports = {
  networks: {
    hardhat: {
      accounts: {
        mnemonic: process.env.SEED_PHRASE,
      },
      chainId: 1337
    },
  },
};
```
- Run `npx hardhat node` to run Hardhat Network and expose a JSON-RPC interface.
- You can now connect MetaMask to your Hardhat Network RPC URL, http://127.0.0.1:8545/. In the MetaMask extension:

    - In the upper left corner, select the network you're currently connected to.
    - Select Add network.
    - Select Add a network manually.
    - Enter your Hardhat Network RPC URL, http://127.0.0.1:8545/ (or http://localhost:8545).
    - Enter your Hardhat Network chain ID, 1337 (or 0x539 in hexadecimal format).
> Alternatively, you can add Hardhat Network to MetaMask using wallet_addEthereumChain in the interactive API playground.

- Reset your local nonce calculation: Mỗi lần restart lại blockchain network này, cần phải reset account ở metamask
### 4/ Interacting With Blockchain with web3js

#### 4.1/ Send transactions

https://docs.metamask.io/wallet/how-to/send-transactions/


Transaction parameters:
- Value:
    - is a hex-encoded value of the network's native currency to send
    - recommend using BN.js/bignumber.js when manipulating values intended for Ethereum.

```js
sendEthButton.addEventListener('click', (e) => {
    ethereum.request({
        method: 'eth_sendTransaction',
        // The following sends an EIP-1559 transaction. Legacy transactions are also supported.
        params: [
          {
            from: accounts[0], // The user's active address.
            to: accounts[1],// Required except during contract publications.
            value: "0x38D7EA4C68000", // Only required to send ether to the recipient from the initiating external account.
        //    gasLimit: '0x5028', // Customizable by the user during MetaMask confirmation.
        //    maxPriorityFeePerGas: '0x3b9aca00', // Customizable by the user during MetaMask confirmation.
        //    maxFeePerGas: '0x2540be400', // Customizable by the user during MetaMask confirmation.
          },
        ],
      })
      .then((txHash) => console.log(`txHash: ${txHash}`))
      .catch((error) => console.log(error));
});
```


#### 4.2/ Sign data:

https://docs.metamask.io/wallet/how-to/sign-data/

- Use eth_signTypedData_v4
- Use personal_sign: ký 1 message đơn giản bằng private key

- Install `eth-sig-util`: A small collection of Ethereum signing functions.

```bash
npm install @metamask/eth-sig-util
```

- Got error:

> BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default.
> This is no longer the case. Verify if you need this module and configure a polyfill for it.


- Install `node-polyfill-webpack-plugin` and the following to your `webpack.config.js`:

```js
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")

module.exports = {
    // Other rules...
    plugins: [
        new NodePolyfillPlugin()
    ]
}
```

```js
personalSignButton.addEventListener('click', async function (event) {
  event.preventDefault();
  const exampleMessage = 'Example `personal_sign` message.';
  try {
    const from = accounts[0];
    // For historical reasons, you must submit the message to sign in hex-encoded UTF-8.
    // This uses a Node.js-style buffer shim in the browser.
    const msg = `0x${Buffer.from(exampleMessage, 'utf8').toString('hex')}`;
    const sign = await ethereum.request({
      method: 'personal_sign',
      params: [msg, from],
    });
    personalSignResult.innerHTML = sign;
    personalSignVerify.disabled = false;
  } catch (err) {
    console.error(err);
    personalSign.innerHTML = `Error: ${err.message}`;
  }
});
```

- `personal_sign` prepends the message with \x19Ethereum Signed Message:\n<length of message> before hashing and signing it.

Ngoài ra, trong các dự án thường sử dụng thư viện `etherjs` và `web3js` để verify chữ ký:

- Example with 'etherjs'

```js
const ethers = require('ethers');

const msg = "Hello, world!"; // Thông điệp đã ký
const signature = "0xYourPersonalSignature"; // Chữ ký cá nhân bạn đã nhận được
const signerAddress = ethers.utils.recoverPersonalSignature({ data: msg, sig: signature });

console.log("Địa chỉ của người ký:", signerAddress);
```
- Với web3js:

```js
const Web3 = require('web3');
const web3 = new Web3();

const msg = "Hello, world!"; // Thông điệp đã được ký
const signature = "0xYourPersonalSignature"; // Chữ ký cá nhân bạn đã nhận được
const signerAddress = web3.eth.accounts.recover(msg, signature);

console.log("Địa chỉ của người ký:", signerAddress);
```
#### 4.3/ Interact with smart contracts

##### Init hardhat-project and deploy sample Smart Contract

https://gitlab-new.bap.jp/RDC/TRAINING/blockchain/02-sample-smartcontract-hardhat-project

##### Get balances

- Adding [web3js](https://web3js.readthedocs.io/en/v1.2.0/getting-started.html) module and adding get balance

```bash
npm install web3
```

- Get balances

```js
import Web3 from 'web3';
// Initialize Web3 with the current provider (usually provided by MetaMask).
const web3 = new Web3(window.ethereum);

// Get the Ethereum address whose balance you want to check
const address = '0xSomeEthereumAddress';

// Fetch the balance in Wei (the smallest unit of ether)
web3.eth.getBalance(address)
  .then((balanceWei) => {
    const balanceEther = web3.utils.fromWei(balanceWei, 'ether');
    console.log(`Balance of ${address} is ${balanceEther} ETH`);
  })
  .catch((error) => {
    console.error('Error fetching balance:', error);
  });
```

##### Interact with smart contracts using Web3js

https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html

Trước hết, bạn cần cài đặt Web3.js và cấu hình nó để kết nối với mạng Ethereum hoặc MetaMask. Sau đó, bạn có thể thực hiện các cuộc gọi hàm tới hợp đồng thông minh. Dưới đây là ví dụ mã JavaScript:

```js
const Web3 = require('web3');

// Khởi tạo Web3 và kết nối với mạng Ethereum hoặc MetaMask
const web3 = new Web3(window.ethereum);

// Địa chỉ của hợp đồng thông minh HelloWorld
const contractAddress = '0xContractAddressHere';

// ABI (Application Binary Interface) của hợp đồng
const contractABI = [
  {
    "constant": true,
    "inputs": [],
    "name": "getGreeting",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];

// Tạo một đối tượng hợp đồng thông minh
const contract = new web3.eth.Contract(contractABI, contractAddress);

// Địa chỉ Ethereum của người gửi
const senderAddress = '0xYourEthereumAddress';

// Gọi hàm getGreeting từ hợp đồng thông minh
contract.methods.getGreeting().call({ from: senderAddress }, (error, result) => {
  if (!error) {
    console.log("Lời chào từ hợp đồng:", result);
  } else {
    console.error("Lỗi khi gọi hàm:", error);
  }
});
```

---

##### Interact with smart contracts using Web3js (Demo Implement)

Quay lại với smart contrac đã tạo ở [02-sample-smartcontract-hardhat-project](https://gitlab-new.bap.jp/RDC/TRAINING/blockchain/02-sample-smartcontract-hardhat-project)

- Phát triển code `Lock.sol` thành `LockV2.sol`
- Sau khi build smart contract mẫu là `Lockv2.sol`, `contractABI` có thể được tìm thấy tại file bên dưới:

```
artifacts/contracts/LockV2.sol/LockV2.json
```
- Sử dụng web3js để tương tác với smart contract

```js
import Web3 from 'web3';
import LockSmartContractInterface from './LockV2.json';

web3Instance = new Web3(window.ethereum);

lockContract = new web3Instance.eth.Contract(
      LockSmartContractInterface.abi,
      LOCK_CONTRACT_ADDRESS
);

async function depositETH(amountInWei, lockDurationInSeconds) {
  const senderAddress = currentAccount; // Địa chỉ người gửi
  const tx = await lockContract.methods.deposit(lockDurationInSeconds).send({
    from: senderAddress,
    value: amountInWei,
  });
  console.log("Transaction hash:", tx.transactionHash);
  return tx.transactionHash;
}

async function withdrawETH() {
  const senderAddress = currentAccount; // Địa chỉ người gửi
  const tx = await lockContract.methods.withdraw().send({ from: senderAddress });
  console.log("Transaction hash:", tx.transactionHash);
  return tx.transactionHash;
}
```

##### Listen the event emitted from smart contract in web3js

```js
// Tên của sự kiện bạn muốn lắng nghe (ví dụ: "Deposited")
const eventName = 'Deposited'; // Thay thế bằng tên sự kiện cụ thể của bạn

// Đăng ký lắng nghe sự kiện
lockContract.events[eventName]()
.on('data', (event) => {
    // Xử lý dữ liệu sự kiện ở đây
    console.log('Sự kiện nhận được:', event.returnValues);
})
.on('error', (error) => {
    console.error('Lỗi lắng nghe sự kiện:', error);
});
```

> Lỗi khi sử dụng vs mạng Harthat Network, có thể Harthat Network chưa hỗ trợ
